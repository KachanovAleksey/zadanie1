#include <string>
#include <cstring>
#include "stdafx.h"
#include "iostream"
#include "iterator"
#include "random"
#include "fstream"
#include <sstream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUSSIAN");
	double freq;
	double fd;
	double dur;
	double ampl;
	double phase;
	double disp;
	string line;
	int cnt = 0;
	int k;

	ifstream InputData("D:\Input.txt");
	if (InputData.is_open())
	{
		cout << "���� ������." << endl;
		getline(InputData, line);
		int strl = line.length();
		for (k = 0; k < strl; k++)
		{
			if ((line[k] == ' ') || (line[k] == '\t')) cnt++;
		}
		if (cnt == 5)
		{
			cout << "���������� ������ ������." << endl;
			while (!InputData.eof())
			{

				getline(InputData, line);
				if (line[0] == '/') continue;
				istringstream iss(line);
				iss >> freq >> fd >> dur >> ampl >> phase >> disp;

			}
			float pi = 3.14;
			double s;
			double td;
			int N = round(dur * fd);
			vector <double> Td;
			vector <double> signal;

			const char* OutputData = "D:\data.txt";
			ofstream out(OutputData, ios::binary | ios::out);

			for (int i = 0; i < N; i++)
			{
				td = i / fd;
				Td.push_back(td);
			}

			if (disp != 0)
			{
				const float mean = 0.0;
				const double stddev = sqrt(disp);
				default_random_engine gen;
				normal_distribution <double> noise(mean, stddev);

				for (int n = 0; n < N; n++)
				{
					s = ampl *sin(2 *pi * freq * Td[n] + (pi*phase)/180) + noise(gen);
					signal.push_back(s);
					out.write((char*)(&signal[n]), sizeof(signal[n]));
				}
				out.close();
			}
			else
			{
				for (int n = 0; n < N; n++)
				{
					s = ampl *sin(2 *pi * freq * Td[n] + (pi*phase)/180);
					signal.push_back(s);
					out.write((char*)(&signal[n]), sizeof(signal[n]));
				}
				out.close();
			}
		}
		else
		{
			cout << "�� ���������� ������ �����." << endl;
		}
	}
	else
	{
		cout << "�� ������� ������� ����." << endl;
	}
	system("pause");
	return 0;
}

